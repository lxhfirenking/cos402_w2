import java.util.*;

public class TicCount {
    private static class Node {
        String buff;
        String [] entry;
        
        public Node (String buff, String[] entry) {
            this.buff = buff;
            this.entry = entry;
        }
    }
    
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        ArrayList<List<Node>> resultList = new ArrayList<List<Node>>();
        for (int i = 0; i < 9; i++) {
            resultList.add(new ArrayList<Node>());
        }
        
        while (scanner.hasNextLine()) {
            String buff = scanner.nextLine();
            String [] entry = buff.trim().split(",");
            
            int counter = 0;
            for (String s:entry) {
                if (s.equals("b")) counter++;
            }
            Node newEntry = new Node(buff, entry);
            if (!present(resultList.get(9 - counter - 1), newEntry))
                resultList.get(9 - counter - 1).add(newEntry);
        }
        scanner.close();
        
        int resultCount[] = new int[9];
        for (int i = 0; i < 9; i++) {
            resultCount[i] = resultList.get(i).size();
            System.out.printf("%d cases in %d step endboard\n", resultCount[i], i + 1);
            /*
            for (String s:resultList.get(i)) {
                System.out.printf("%s\n", s);
            }
            */
        }
         int overallResult = resultCount[4]*3*2*2 +
                            resultCount[5]*3*2*3*2 + 
                            resultCount[6]*4*3*2*3*2 +
                            resultCount[7]*4*3*2*4*3*2 +
                            resultCount[8]*4*3*2*5*4*3*2;
        System.out.printf("Overall number of game tree is %d*3!*2! + %d*3!*3! + %d*3!*4! + %d*4!*4! * %d*4!*5! = %d\n",
                resultCount[4], resultCount[5], resultCount[6], resultCount[7], resultCount[8], overallResult);
        
        

    }
   
    /* check for symmetry*/
    public static boolean present(List<Node> list, Node check) {
        String[] a = check.entry;
        for (Node x:list) {
            String[] b = x.entry;
            if (a[0].equals(b[0]) && a[1].equals(b[1]) && a[2].equals(b[2]) &&
                a[3].equals(b[3]) && a[4].equals(b[4]) && a[5].equals(b[5]) &&
                a[6].equals(b[6]) && a[7].equals(b[7]) && a[8].equals(b[8])) return true;
            if (a[2].equals(b[0]) && a[1].equals(b[1]) && a[0].equals(b[2]) &&
                a[5].equals(b[3]) && a[4].equals(b[4]) && a[3].equals(b[5]) &&
                a[8].equals(b[6]) && a[7].equals(b[7]) && a[6].equals(b[8])) return true;
            if (a[6].equals(b[0]) && a[7].equals(b[1]) && a[8].equals(b[2]) &&
                a[3].equals(b[3]) && a[4].equals(b[4]) && a[5].equals(b[5]) &&
                a[0].equals(b[6]) && a[1].equals(b[7]) && a[2].equals(b[8])) return true;
            if (a[0].equals(b[0]) && a[3].equals(b[1]) && a[6].equals(b[2]) &&
                a[1].equals(b[3]) && a[4].equals(b[4]) && a[7].equals(b[5]) &&
                a[2].equals(b[6]) && a[5].equals(b[7]) && a[8].equals(b[8])) return true;
            if (a[8].equals(b[0]) && a[5].equals(b[1]) && a[2].equals(b[2]) &&
                a[7].equals(b[3]) && a[4].equals(b[4]) && a[1].equals(b[5]) &&
                a[6].equals(b[6]) && a[3].equals(b[7]) && a[0].equals(b[8])) return true;
            if (a[2].equals(b[0]) && a[5].equals(b[1]) && a[8].equals(b[2]) &&
                a[1].equals(b[3]) && a[4].equals(b[4]) && a[7].equals(b[5]) &&
                a[0].equals(b[6]) && a[3].equals(b[7]) && a[6].equals(b[8])) return true;
            if (a[6].equals(b[0]) && a[3].equals(b[1]) && a[0].equals(b[2]) &&
                a[7].equals(b[3]) && a[4].equals(b[4]) && a[1].equals(b[5]) &&
                a[8].equals(b[6]) && a[5].equals(b[7]) && a[2].equals(b[8])) return true;
            if (a[8].equals(b[0]) && a[5].equals(b[1]) && a[2].equals(b[2]) &&
                a[7].equals(b[3]) && a[4].equals(b[4]) && a[1].equals(b[5]) &&
                a[6].equals(b[6]) && a[3].equals(b[7]) && a[0].equals(b[8])) return true;
            if (a[8].equals(b[0]) && a[7].equals(b[1]) && a[6].equals(b[2]) &&
                a[5].equals(b[3]) && a[4].equals(b[4]) && a[3].equals(b[5]) &&
                a[2].equals(b[6]) && a[1].equals(b[7]) && a[0].equals(b[8])) return true;
        }
        return false;
    }
}